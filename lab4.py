#!/usr/bin/env python3
import sys
import math
import numpy as matlab2

from glfw.GLFW import *

from OpenGL.GL import *
from OpenGL.GLU import *

# Huh, my pendrive with original code decided to commit honourable sudoku like chinese emperor Tiramisu...
# Here comes writing that  code again
#
# I don't quite understand what did You mean by the last point for 4.5 grade...
# So I implemented first person view 'freecam' mode and orbitting around origin.
# You can change them by pressing spacebar
# freecam movement by mouse and WSAD
# going back to orbit mode works like viewer was looking somewhere else and then quick focused "look what a wonderful weld! (on the bomb)" 

look_direction = matlab2.zeros(3)
position = matlab2.zeros(3)

theta = 0.0
phi = 0.0
scale=10.0
pix2angle = 1
pix2scale = 0.25
move_time_multiplier = 2.0
orientation = 1.0

mode_freecam = False
left_mouse_button_pressed = 0
right_mouse_button_pressed = 0
move_time = None
move_direction = None
mouse_x_pos_old = 0
mouse_y_pos_old = 0
delta_x = 0
delta_y = 0


def startup():
    update_viewport(None, 400, 400)
    glClearColor(0.0, 0.0, 0.0, 1.0)
    glEnable(GL_DEPTH_TEST)
    
    if(mode_freecam):
        print("Running in freecam mode")
    else:
        print("Running in orbit mode")
    print("press spacebar to change mode")
    print("right click and moving cursor up/down will change scale (distance) when in orbit mode")

def shutdown():
    pass


def axes():
    glBegin(GL_LINES)

    glColor3f(1.0, 0.0, 0.0)
    glVertex3f(-5.0, 0.0, 0.0)
    glVertex3f(5.0, 0.0, 0.0)

    glColor3f(0.0, 1.0, 0.0)
    glVertex3f(0.0, -5.0, 0.0)
    glVertex3f(0.0, 5.0, 0.0)

    glColor3f(0.0, 0.0, 1.0)
    glVertex3f(0.0, 0.0, -5.0)
    glVertex3f(0.0, 0.0, 5.0)

    glEnd()


def example_object():
    glColor3f(1.0, 1.0, 1.0)

    quadric = gluNewQuadric()
    gluQuadricDrawStyle(quadric, GLU_LINE)
    glRotatef(90, 1.0, 0.0, 0.0)
    glRotatef(-90, 0.0, 1.0, 0.0)

    gluSphere(quadric, 1.5, 10, 10)

    glTranslatef(0.0, 0.0, 1.1)
    gluCylinder(quadric, 1.0, 1.5, 1.5, 10, 5)
    glTranslatef(0.0, 0.0, -1.1)

    glTranslatef(0.0, 0.0, -2.6)
    gluCylinder(quadric, 0.0, 1.0, 1.5, 10, 5)
    glTranslatef(0.0, 0.0, 2.6)

    glRotatef(90, 1.0, 0.0, 1.0)
    glTranslatef(0.0, 0.0, 1.5)
    gluCylinder(quadric, 0.1, 0.0, 1.0, 5, 5)
    glTranslatef(0.0, 0.0, -1.5)
    glRotatef(-90, 1.0, 0.0, 1.0)

    glRotatef(-90, 1.0, 0.0, 1.0)
    glTranslatef(0.0, 0.0, 1.5)
    gluCylinder(quadric, 0.1, 0.0, 1.0, 5, 5)
    glTranslatef(0.0, 0.0, -1.5)
    glRotatef(90, 1.0, 0.0, 1.0)

    glRotatef(90, 0.0, 1.0, 0.0)
    glRotatef(-90, 1.0, 0.0, 0.0)
    gluDeleteQuadric(quadric)

# more like process all the stuff now...
def render(time):
    global theta
    global phi
    global scale
    global look_direction
    global move_time
    global position
    global delta_x
    global delta_y
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    glLoadIdentity()

    if left_mouse_button_pressed:
        theta += delta_x * pix2angle * math.pi / 180
        phi += delta_y * pix2angle * math.pi / 180
        
        #not truly necessary
        theta %= math.pi*2
        
        #sketchy as frigg elevation math
        #could have limited going up/down instead
        global orientation
        if phi > math.pi/2:
            orientation *= -1.0
            phi -= math.pi
        elif phi < -math.pi/2:
            orientation *= -1.0
            phi -= -math.pi
        
    if right_mouse_button_pressed:
        #(delta_x + delta_y)*pix2scale is **good enough** approximation IMO
        #minus instead of + so moving mouse down will decrease scale
        scale +=  delta_y * pix2scale
        if(scale<0.001):
            scale = 0.001
        #infinity sounds like great upper boundary. I challenge You to reach it without changing parameters.
    
    delta_x = 0
    delta_y = 0
    
    if mode_freecam and move_direction != None:
        dt = move_time - time
        move_time = time
        look_dir = look_direction
        #create perpendicular vector
        if move_direction == GLFW_KEY_A or move_direction == GLFW_KEY_D:
            look_dir = matlab2.cross(look_dir,[0,1.0,0])
        #make it unit vector
        look_dir /=  (look_dir**2).sum()**0.5 
        #and scale according to elapsed time
        #thus making movement speed independent from program run speed
        look_dir *= dt * move_time_multiplier
        
        if(move_direction == GLFW_KEY_W) or (move_direction == GLFW_KEY_D):
            position = matlab2.add(position, look_dir)
        elif(move_direction == GLFW_KEY_S) or (move_direction == GLFW_KEY_A):
            position = matlab2.subtract(position, look_dir)
            
    
    look_direction = matlab2.array([ scale*math.cos(theta)*math.cos(phi), 
                                    scale*math.sin(phi), 
                                    scale*math.sin(theta)*math.cos(phi) * orientation])
    
    #print(look_direction)

    if(mode_freecam == False):
        gluLookAt(look_direction[0], look_direction[1], look_direction[2],
                  0.0, 0.0, 0.0, 
                  0.0, orientation, 0.0)
    else:
        gluLookAt(position[0], position[1], position[2],
                    -look_direction[0], -look_direction[1], -look_direction[2],
                    0.0, orientation, 0.0)
    
    #probably these should use theta/(math.pi / 180), phi like so too
    #glRotatef(theta, 0.0, 1.0, 0.0)
    #glRotatef(phi, 1.0, 0.0, 0.0)
    #glScalef(scale, scale, scale)

    axes()
    example_object()

    glFlush()


def update_viewport(window, width, height):
    global pix2angle
    pix2angle = 360.0 / width

    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()

    gluPerspective(70, 1.0, 0.1, 300.0)

    if width <= height:
        glViewport(0, int((height - width) / 2), width, width)
    else:
        glViewport(int((width - height) / 2), 0, height, height)

    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()

def keyboard_process_move(key, glfw_press):
    global move_direction
    global move_time
    if glfw_press == GLFW_PRESS:
        move_time = glfwGetTime()
        move_direction = key
    elif move_direction == key and glfw_press == GLFW_RELEASE:
        move_direction = None
    
    
def keyboard_key_callback(window, key, scancode, action, mods):
    global phi
    global theta
    
    if key == GLFW_KEY_ESCAPE and action == GLFW_PRESS:
        glfwSetWindowShouldClose(window, GLFW_TRUE)
    elif key == GLFW_KEY_SPACE and action == GLFW_PRESS:
        global mode_freecam
        global position
        global look_direction
        mode_freecam = not mode_freecam
        if(mode_freecam):
            position = look_direction
            look_direction = matlab2.zeros(3)
            print("Running in freecam mode")
        else:
            #position to direction conversion
            phi = math.asin(position[1]/scale)
            theta = math.acos(position[0]/math.cos(phi)/scale)
            print("Running in orbit mode")
            
    elif key == GLFW_KEY_W or key == GLFW_KEY_S or key == GLFW_KEY_A or key == GLFW_KEY_D:
        keyboard_process_move(key,action)
        
        
        
def mouse_motion_callback(window, x_pos, y_pos):
    global delta_x
    global delta_y
    global mouse_x_pos_old
    global mouse_y_pos_old

    delta_x = x_pos - mouse_x_pos_old
    mouse_x_pos_old = x_pos
    
    delta_y = y_pos - mouse_y_pos_old
    mouse_y_pos_old = y_pos


def mouse_button_callback(window, button, action, mods):
    global left_mouse_button_pressed
    global right_mouse_button_pressed

    left_mouse_button_pressed = (button == GLFW_MOUSE_BUTTON_LEFT and action == GLFW_PRESS)
    
    #only in orbit mode
    right_mouse_button_pressed  = (button == GLFW_MOUSE_BUTTON_RIGHT and action == GLFW_PRESS and mode_freecam == False)


def main():
    if not glfwInit():
        sys.exit(-1)

    window = glfwCreateWindow(400, 400, "Bartosz Hornicki, Fallout 3: Megaton atomic bomb mesh viewer", None, None)
    #Huh, megaton is like Morrowind's Baar Dau (that flying rock in Vivec city)
    #But instead of one village it'll doom entire Vvardenfell and part of continent
    #I wonder if dunmers known that instead only potential energy it was frozen in space with its entire kinetic energy too
    #Gonna have to check it
    #Yup, after 2E 582 Vekh' energy 'blackout' they did know effects of falling fragments impact energy and after disappearance of Vekh they created ingenium to stabilise Baar Dau
    if not window:
        glfwTerminate()
        sys.exit(-1)

    glfwMakeContextCurrent(window)
    glfwSetFramebufferSizeCallback(window, update_viewport)
    glfwSetKeyCallback(window, keyboard_key_callback)
    glfwSetCursorPosCallback(window, mouse_motion_callback)
    glfwSetMouseButtonCallback(window, mouse_button_callback)
    glfwSwapInterval(1)

    startup()
    while not glfwWindowShouldClose(window):
        render(glfwGetTime())
        glfwSwapBuffers(window)
        glfwPollEvents()
    shutdown()

    glfwTerminate()


if __name__ == '__main__':
    main()
