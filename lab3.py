#!/usr/bin/env python3
import sys

from glfw.GLFW import *

from OpenGL.GL import *
from OpenGL.GLU import *

import numpy as matlab2
import math
import random
from datetime import datetime

N = 31

points = matlab2.zeros((N, N, 3))
seed = datetime.now()

def calculate_points():
    global points
    u = matlab2.zeros(N)
    v = matlab2.zeros(N)
    tmp=1/(N-1)
    for i in range(N):
        u[i] = tmp*i
        v[i] = tmp*i
    for i in range(N):
        for j in range(N):
            tu = u[i]
            tv = v[j]
            xz = -90 * tu**5 + 225 * tu**4 - 270 * tu**3 + 180 * tu**2 - 45 * tu
            piv = math.pi * tv
            points[i][j][0] = xz * math.cos(piv)
            points[i][j][1] = 160 * tu**4 - 320 * tu**3 + 160 * tu**2
            points[i][j][2] = xz * math.sin(piv)


def startup():
    update_viewport(None, 400, 400)
    glClearColor(0.0, 0.0, 0.0, 1.0)
    glEnable(GL_DEPTH_TEST)
        
    calculate_points()


def shutdown():
    pass

def spin(angle):
    glRotatef(angle, 1.0, 0.0, 0.0)
    glRotatef(angle, 0.0, 1.0, 0.0)
    glRotatef(angle, 0.0, 0.0, 1.0)


def axes():
    glBegin(GL_LINES)

    glColor3f(1.0, 0.0, 0.0)
    glVertex3f(-5.0, 0.0, 0.0)
    glVertex3f(5.0, 0.0, 0.0)

    glColor3f(0.0, 1.0, 0.0)
    glVertex3f(0.0, -5.0, 0.0)
    glVertex3f(0.0, 5.0, 0.0)

    glColor3f(0.0, 0.0, 1.0)
    glVertex3f(0.0, 0.0, -5.0)
    glVertex3f(0.0, 0.0, 5.0)

    glEnd()

def jajo():
    random.seed(seed)
    color_bottom =(random.random(), random.random(), random.random())
    color_top =(random.random(), random.random(), random.random())
    for i in range(N-1):
        glBegin(GL_TRIANGLE_STRIP)
        random.seed(seed)
        for j in range(N):
            
            if(j==N-1):
                #same colors on vertical seam
                random.seed(seed)
            
            if(i==0):
                #same color in bottom point
                glColor3fv(color_bottom)
            elif(i==math.floor(N/2) or i==math.floor((N-1)/2)):
                #same color in top point/layer
                glColor3fv(color_top)
            else:
                glColor3f(random.random(), random.random(), random.random())
            glVertex(points[i][j][0], points[i][j][1], points[i][j][2])
            
            if(i==0 or i==math.floor(N/2)):
                # after same color bottom or top point go back to random color scheme
                glColor3f(random.random(), random.random(), random.random())
            elif(i==math.floor((N-1)/2-1)):
                #to top point
                glColor3fv(color_top)
            elif(i==N-2):
                #to bottom
                glColor3fv(color_bottom)
            glVertex(points[i+1][j][0], points[i+1][j][1], points[i+1][j][2])
        glEnd()
    
    glFlush()
    
def render(time):
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    glLoadIdentity()
    spin(time*180/math.pi)
    axes()
    
    jajo()


def update_viewport(window, width, height):
    if width == 0:
        width = 1
    glFlush()


def update_viewport(window, width, height):
    if width == 0:
        width = 1
    if height == 0:
        height = 1
    aspect_ratio = width / height

    glMatrixMode(GL_PROJECTION)
    glViewport(0, 0, width, height)
    glLoadIdentity()

    if width <= height:
        glOrtho(-7.5, 7.5, -7.5 / aspect_ratio, 7.5 / aspect_ratio, 7.5, -7.5)
    else:
        glOrtho(-7.5 * aspect_ratio, 7.5 * aspect_ratio, -7.5, 7.5, 7.5, -7.5)

    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()


def main():
    if not glfwInit():
        sys.exit(-1)

    window = glfwCreateWindow(400, 400, "Bartosz Hornicki Jajo Viewer 1.23.4", None, None)
    if not window:
        glfwTerminate()
        sys.exit(-1)

    glfwMakeContextCurrent(window)
    glfwSetFramebufferSizeCallback(window, update_viewport)
    glfwSwapInterval(1)

    startup()
    while not glfwWindowShouldClose(window):
        render(glfwGetTime())
        glfwSwapBuffers(window)
        glfwPollEvents()
    shutdown()

    glfwTerminate()


if __name__ == '__main__':
    main()
