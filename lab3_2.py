#!/usr/bin/env python3
import sys

from glfw.GLFW import *

from OpenGL.GL import *
from OpenGL.GLU import *
import numpy as matlab2
import math
import random

N = 31
R = 1.2 #donut radius from center to middle of cross-section
r = .3  #donut cross-section radius
#I know it's called differently than donut

points = matlab2.zeros((N, N, 3))
c1 = (random.random(), random.random(), random.random())
c2 = (random.random(), random.random(), random.random())

def calculate_points():
    global points
    u = matlab2.zeros(N)
    v = matlab2.zeros(N)
    tmp=1/(N-1)
    for i in range(N):
        u[i] = tmp*i
        v[i] = tmp*i
    for i in range(N):
        for j in range(N):
            two_pi_u=2*math.pi*u[i]
            two_pi_v=2*math.pi*v[j]
            points[i][j][0] = (R + r*math.cos(two_pi_v))*math.cos(two_pi_u)
            points[i][j][1] = (R + r*math.cos(two_pi_v))*math.sin(two_pi_u)
            points[i][j][2] = r*math.sin(two_pi_v)

def startup():
    update_viewport(None, 400, 400)
    glClearColor(0.0, 0.0, 0.0, 1.0)
    glEnable(GL_DEPTH_TEST)
    calculate_points()

def shutdown():
    pass

def spin(angle):
    glRotatef(angle, 1.0, 0.0, 0.0)
    glRotatef(angle, 0.0, 1.0, 0.0)
    glRotatef(angle, 0.0, 0.0, 1.0)


def axes():
    glBegin(GL_LINES)

    glColor3f(1.0, 0.0, 0.0)
    glVertex3f(-5.0, 0.0, 0.0)
    glVertex3f(5.0, 0.0, 0.0)

    glColor3f(0.0, 1.0, 0.0)
    glVertex3f(0.0, -5.0, 0.0)
    glVertex3f(0.0, 5.0, 0.0)

    glColor3f(0.0, 0.0, 1.0)
    glVertex3f(0.0, 0.0, -5.0)
    glVertex3f(0.0, 0.0, 5.0)

    glEnd()

def donut(offset_x=0, offset_y=0, offset_z=0):
    glBegin(GL_TRIANGLE_STRIP)
    for y in range(N):
        for x in range(N):
            glColor3fv(c1)
            glVertex(points[x][y-1][0]+offset_x, points[x][y-1][1]+offset_y, points[x][y-1][2]+offset_z)
            glVertex(points[x][y][0]+offset_x, points[x][y][1]+offset_y, points[x][y][2]+offset_z)
    glEnd()

def donutVertical(offset_x=0, offset_y=0, offset_z=0):
    glBegin(GL_TRIANGLE_STRIP)
    for y in range(N):
        for x in range(N):
            glColor3fv(c2)
            glVertex(points[x][y-1][0]+offset_x, points[x][y-1][2]+offset_y, points[x][y-1][1]+offset_z)
            glVertex(points[x][y][0]+offset_x, points[x][y][2]+offset_y, points[x][y][1]+offset_z)
    glEnd()

def render(time):
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    glLoadIdentity()
    spin(time*180/3.1415)
    axes()

    offset = 2*(R-r)
    
    donut()
    donut(2*offset)
    donut(-2*offset)

    donutVertical(offset)
    donutVertical(-offset)
    
    glFlush()


def update_viewport(window, width, height):
    if width == 0:
        width = 1
    if height == 0:
        height = 1
    aspect_ratio = width / height

    glMatrixMode(GL_PROJECTION)
    glViewport(0, 0, width, height)
    glLoadIdentity()

    if width <= height:
        glOrtho(-7.5, 7.5, -7.5 / aspect_ratio, 7.5 / aspect_ratio, 7.5, -7.5)
    else:
        glOrtho(-7.5 * aspect_ratio, 7.5 * aspect_ratio, -7.5, 7.5, 7.5, -7.5)

    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()


def main():
    if not glfwInit():
        sys.exit(-1)

    window = glfwCreateWindow(400, 400, "Bartosz Hornicki - Curse Of The Crystal Donut", None, None)
    if not window:
        glfwTerminate()
        sys.exit(-1)

    glfwMakeContextCurrent(window)
    glfwSetFramebufferSizeCallback(window, update_viewport)
    glfwSwapInterval(1)

    startup()
    while not glfwWindowShouldClose(window):
        render(glfwGetTime())
        glfwSwapBuffers(window)
        glfwPollEvents()
    shutdown()

    glfwTerminate()


if __name__ == '__main__':
    main()
