#!/usr/bin/env python3
import sys
import math
import numpy as matlab2

from glfw.GLFW import *

from OpenGL.GL import *
from OpenGL.GLU import *

egg_N = 69  #Nice.
egg_points = matlab2.zeros((egg_N, egg_N, 3))
egg_normals = matlab2.zeros((egg_N, egg_N, 3))
display_normals = True

theta = 0.0
phi = 0.0
distance = 10.0
pix2angle = 2.0
pix2distance = 1.0

left_mouse_button_pressed = False
right_mouse_button_pressed = False
mouse_x_pos_old = 0
mouse_y_pos_old = 0
delta_x = 0
delta_y = 0

mat_ambient = [1.0, 1.0, 1.0, 1.0]
mat_diffuse = [1.0, 1.0, 1.0, 1.0]
mat_specular = [1.0, 1.0, 1.0, 1.0]
mat_shininess = 20.0

light0_ambient = [0.1, 0.1, 0.0, 1.0]
light0_diffuse = [0.8, 0.8, 0.0, 1.0]
light0_specular = [1.0, 1.0, 1.0, 1.0]
light0_position = [0.0, 0.0, 10.0, 1.0]

light1_ambient = [0.1, 0.0, 0.0, 1.0]
light1_diffuse = [0.8, 0.0, 0.8, 1.0]
light1_specular = [1.0, 0.0, 0.1, 1.0]
light1_position = [10.0, 10.0, 0.0, 1.0]

att_constant = 1.0
att_linear = 0.05
att_quadratic = 0.001

param_index = 0
param_index_max = 4*4*2
param_change_time = 0

#can someone give me a pointer?
#edit: no way, it works!
param_array = [
    light0_ambient ,
    light0_diffuse ,
    light0_specular,
    light0_position,
    light1_ambient ,
    light1_diffuse ,
    light1_specular,
    light1_position
]
#only one light source is for noobs if we use this approach
param_names_array = [
    "light0_ambient ",
    "light0_diffuse ",
    "light0_specular",
    "light0_position",
    "light1_ambient ",
    "light1_diffuse ",
    "light1_specular",
    "light1_position"
]
 

def print_parameters():
    x = math.floor(param_index/4)
    y = param_index%4
    for i in range(len(param_array)):   #yup.
        print(param_names_array[i],end="\t")
        for j in range(4):
            if(i==x and j==y):
                print("$%1.1f$, "%param_array[i][j],end="\t")
            else:
                print("%1.1f, "%param_array[i][j],end="\t")
        print("")
    print("")

def set_lights():
    #if it's dumb, but it works, then it ain't dumb
    glLightfv(GL_LIGHT0, GL_AMBIENT,  light0_ambient)
    glLightfv(GL_LIGHT0, GL_DIFFUSE,  light0_diffuse)
    glLightfv(GL_LIGHT0, GL_SPECULAR, light0_specular)

    glLightfv(GL_LIGHT1, GL_AMBIENT,  light1_ambient)
    glLightfv(GL_LIGHT1, GL_DIFFUSE,  light1_diffuse)
    glLightfv(GL_LIGHT1, GL_SPECULAR, light1_specular)

#process pressing arrow keys
def change_parameters(key, action):
    global param_index
    global param_array
    global param_change_time
    time = glfwGetTime() 
    if(time - param_change_time < .1 and action != GLFW_PRESS):
        return
    param_change_time = time
    
    x = math.floor(param_index/4)
    y = param_index%4
    
    #change index
    if(key == GLFW_KEY_RIGHT):
        param_index = (param_index + 1) % param_index_max
        x = math.floor(param_index/4)
        y = param_index%4
    elif(key == GLFW_KEY_LEFT and action != GLFW_PRESS):
        param_index = (param_index + param_index_max - 1) % param_index_max
        x = math.floor(param_index/4)
        y = param_index%4
        
    #update value
    elif(key == GLFW_KEY_UP):
        param_array[x][y] += .1 
        if(param_array[x][y] > 1.0
           and not (x == 3 or x == 7)):
                param_array[x][y] = 1.0
    elif(key == GLFW_KEY_DOWN):
        param_array[x][y] -= .1 
        if(param_array[x][y] < .0
           and not (x == 3 or x == 7)):
                param_array[x][y] = .0
    
    #pressed different key
    else:
        return

    print_parameters()
    set_lights()
    


def startup():
    update_viewport(None, 400, 400)
    glClearColor(0.0, 0.0, 0.0, 1.0)
    glEnable(GL_DEPTH_TEST)

    glMaterialfv(GL_FRONT, GL_AMBIENT, mat_ambient)
    glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_diffuse)
    glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular)
    glMaterialf(GL_FRONT, GL_SHININESS, mat_shininess)

    set_lights()
    
    #glLightfv(GL_LIGHT0, GL_POSITION, light0_position)
    #glLightfv(GL_LIGHT1, GL_POSITION, light1_position)
    
    glLightf(GL_LIGHT0, GL_CONSTANT_ATTENUATION, att_constant)
    glLightf(GL_LIGHT0, GL_LINEAR_ATTENUATION, att_linear)
    glLightf(GL_LIGHT0, GL_QUADRATIC_ATTENUATION, att_quadratic)


    glLightf(GL_LIGHT1, GL_CONSTANT_ATTENUATION, att_constant)
    glLightf(GL_LIGHT1, GL_LINEAR_ATTENUATION, att_linear)
    glLightf(GL_LIGHT1, GL_QUADRATIC_ATTENUATION, att_quadratic)

    glShadeModel(GL_SMOOTH)
    glEnable(GL_LIGHTING)
    glEnable(GL_LIGHT0)
    glEnable(GL_LIGHT1)

    print("Use left and right arrows to change selected parameter.\n"
          "Use up and down arrows to change parameter value.\n"
          "Press space to toggle normals")
    print_parameters()
    calculate_egg()

def shutdown():
    pass

#calculate egg points and normal vectors
#this function just asks for multithreading
def calculate_egg():
    global egg_points
    N = egg_N
    u = matlab2.zeros(N)
    v = matlab2.zeros(N)
    tmp=1/(N-1)
    for i in range(N):
        u[i] = tmp*i
        v[i] = tmp*i
    for i in range(N):
        for j in range(N):
            tu = u[i]
            tv = v[j]
            piv = math.pi * tv
            spiv = math.sin(piv)
            cpiv = math.cos(piv)
            #points
            xz = -90 * tu**5 + 225 * tu**4 - 270 * tu**3 + 180 * tu**2 - 45 * tu
            egg_points[i][j] = [xz * cpiv,
                                160 * tu**4 - 320 * tu**3 + 160 * tu**2,
                                xz * spiv]
            
            #normals
            temp = (-450 * tu**4 + 900 * tu**3 - 810 * tu**2 + 360 * tu - 45)
            
            xu = temp * cpiv
            xv = math.pi * -xz * spiv
            yu = 640 * tu**3 - 960 * tu**2 + 320 * tu
            yv = 0
            zu = temp * spiv
            zv = math.pi * xz * cpiv
            
            egg_normals[i][j] = [yu*zv - zu*yv,
                                 zu*xv - xu*zv,
                                 xu*yv - yu*xv]
            
            #Who doesn't love brute force?
            if(i==0 or i==N-1):
                egg_normals[i][j] = [0., -1., 0.]
            elif(N%2 and i==math.floor(N/2)):
                egg_normals[i][j] = [0., 1., 0.]
            else:
                #make it an unit vector
                egg_normals[i][j] /=  (egg_normals[i][j]**2).sum()**0.5 
                
            if(i>=N/2 and i != N-1):
                egg_normals[i][j] *= -1
    #print(egg_normals)

def jajo():
    for i in range(egg_N-1):
        glBegin(GL_TRIANGLE_STRIP)
        for j in range(egg_N):
            
            glNormal3fv(egg_normals[i][j])
            #Yay, better than glVertex and repeat yourself 3 times
            #Found this out while searching for glNormalfv version
            glVertex3fv(egg_points[i][j])
            
            glNormal3fv(egg_normals[i+1][j])
            glVertex3fv(egg_points[i+1][j])
        glEnd()
        
    if(display_normals):
        for i in range(egg_N):
            for j in range(egg_N):
                glBegin(GL_LINES)
                glVertex3fv(egg_points[i][j])
                glVertex3fv(egg_points[i][j] + egg_normals[i][j])
                glEnd()

def render(time):
    global theta
    global phi
    global distance
    global delta_x
    global delta_y

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

    glLoadIdentity()
    viewer = matlab2.array([ distance*math.cos(theta)*math.cos(phi), 
                            distance*math.sin(phi), 
                            distance*math.sin(theta)*math.cos(phi)])
    
    gluLookAt(viewer[0], viewer[1], viewer[2],
              0.0, 0.0, 0.0, 0.0, 1.0, 0.0)


    if left_mouse_button_pressed:
        theta += delta_x * pix2angle * math.pi / 180
        phi += delta_y * pix2angle * math.pi / 180
        
        #not truly necessary
        theta %= math.pi*2
        
        global orientation
        if phi >   math.pi/2:
            phi =  math.pi/2
        elif phi < -math.pi/2:
            phi =  -math.pi/2
    if right_mouse_button_pressed:
        #(delta_x + delta_y)*pix2distance is **good enough** approximation IMO
        #minus instead of + so moving mouse down will decrease distance
        distance +=  delta_y * pix2distance
        if(distance<0.001):
            distance = 0.001
        #infinity sounds like great upper boundary. I challenge You to reach it without changing parameters.
    delta_x = 0
    delta_y = 0

    
    #target
    jajo()
    #quadric = gluNewQuadric()
    #gluQuadricDrawStyle(quadric, GLU_FILL)
    #gluSphere(quadric, 3.0, 10, 10)
    #gluDeleteQuadric(quadric)
    
    #light 0
    glTranslatef(light0_position[0], 
                 light0_position[1], 
                 light0_position[2])
    #huh, shouldn't it be referenced after translation as [.0, .0, .0]?
    glLightfv(GL_LIGHT0, GL_POSITION, light0_position)
    quadric = gluNewQuadric()
    gluQuadricDrawStyle(quadric, GLU_LINE)
    gluSphere(quadric, 0.5, 6, 5)
    gluDeleteQuadric(quadric)
    #Shouldn't glLoadIdentity do the same? Why does it not?
    glTranslatef(-light0_position[0], 
                 -light0_position[1], 
                 -light0_position[2])
    
    #light 1
    glTranslatef(light1_position[0], 
                 light1_position[1], 
                 light1_position[2])
    glLightfv(GL_LIGHT1, GL_POSITION, light1_position)
    quadric = gluNewQuadric()
    gluQuadricDrawStyle(quadric, GLU_LINE)
    gluSphere(quadric, 0.5, 6, 5)
    gluDeleteQuadric(quadric)
    
    glFlush()


def update_viewport(window, width, height):
    global pix2angle
    pix2angle = 360.0 / width

    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()

    gluPerspective(70, 1.0, 0.1, 300.0)

    if width <= height:
        glViewport(0, int((height - width) / 2), width, width)
    else:
        glViewport(int((width - height) / 2), 0, height, height)

    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()


def keyboard_key_callback(window, key, scancode, action, mods):
    global display_normals
    if key == GLFW_KEY_ESCAPE and action == GLFW_PRESS:
        glfwSetWindowShouldClose(window, GLFW_TRUE)
    elif key == GLFW_KEY_SPACE and action == GLFW_PRESS:
        display_normals = not display_normals
    else:
        change_parameters(key,action)


def mouse_motion_callback(window, x_pos, y_pos):
    global delta_x
    global delta_y
    global mouse_x_pos_old
    global mouse_y_pos_old

    delta_x = x_pos - mouse_x_pos_old
    mouse_x_pos_old = x_pos
    
    delta_y = y_pos - mouse_y_pos_old
    mouse_y_pos_old = y_pos


def mouse_button_callback(window, button, action, mods):
    global left_mouse_button_pressed
    global right_mouse_button_pressed
    
    left_mouse_button_pressed = button == GLFW_MOUSE_BUTTON_LEFT and action == GLFW_PRESS
    right_mouse_button_pressed = button == GLFW_MOUSE_BUTTON_RIGHT and action == GLFW_PRESS
    

def main():
    if not glfwInit():
        sys.exit(-1)

    window = glfwCreateWindow(400, 400, "Bartosz Hornicki - Vanishing Light", None, None)
    if not window:
        glfwTerminate()
        sys.exit(-1)

    glfwMakeContextCurrent(window)
    glfwSetFramebufferSizeCallback(window, update_viewport)
    glfwSetKeyCallback(window, keyboard_key_callback)
    glfwSetCursorPosCallback(window, mouse_motion_callback)
    glfwSetMouseButtonCallback(window, mouse_button_callback)
    glfwSwapInterval(1)

    startup()
    while not glfwWindowShouldClose(window):
        render(glfwGetTime())
        glfwSwapBuffers(window)
        glfwPollEvents()
    shutdown()

    glfwTerminate()


if __name__ == '__main__':
    main()
