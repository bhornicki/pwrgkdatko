#!/usr/bin/env python3
import sys
import random as rand
import math

from glfw.GLFW import *
from OpenGL.GL import *
from OpenGL.GLU import *

#Don't judge
RECTANGLE_COLORS = []

#used in sierpinskiIterative quirky optimisation (which is by default commented out)
PIXEL_HALF_SIZE = []

#startup() - preparation before main loop
def startup():
    update_viewport(None, 400, 400)
    glClearColor(0.5, 0.5, 0.5, 1.0)
    
    rand.seed()
    
    #prepare random colors.
    #https://xkcd.com/221/
    global RECTANGLE_COLORS
    for i in range(4):
        RECTANGLE_COLORS += [[rand.random(),rand.random(),rand.random()]]

def shutdown():
    pass

#TODO: check for Doxygen scheme and generator for Python. Also check naming convention
#rectangle() Draws rectangle on the scene.
#x,y - bottom left corner coordinates
#w,h - rectangle width and height
#color - rectangle color. Leave None for "one-time pseudorandom" gradient fill
#Deform - changes rectangle to horizontally aligned trapezoid. 
#         input range: [-1.0, probably infinity]
#         Exceeding these values may end up with "retarded Kotlin logo" effect! 
#         Which of course is not a bug, but a feature.
def rectangle(x,y,w,h,color=None,deform=0):
    offset=math.floor(w*deform/2)
    points = [[x,y],[x-offset,y+h],[x+w,y],[x+w+offset,y+h]]
    
    glBegin(GL_TRIANGLES)
    for i in range(3):
        glColor3fv(RECTANGLE_COLORS[i] if color==None else color)
        glVertex2fv(points[i])
    glEnd()
    
    glBegin(GL_TRIANGLES)
    for i in range(1,4):
        glColor3fv(RECTANGLE_COLORS[i] if color==None else color)
        glVertex2fv(points[i])
    glEnd()
    

#sierpinski() draws Sierpinski rectangle via recurrent subdivision of rectangle. 
#           keeps background unchanged
#x,y - bottom left corner coordinates
#w,h - rectangle width and height
#depth - number of subdividing operations which produce cutout in the middle of rectangle
#color - rectangle color
def sierpinski(x,y,w,h,depth,color):
    if(depth<0):
        raise Exception("Depth cannot be negative")
    if(depth==0):
        rectangle(x,y,w,h,color)
        return
    
    thirdOfH=h/3
    thirdOfW=w/3
    
    for i in range(3):
        for j in range(3):
            if(i==j==1):
                continue
            sierpinski(x+i*thirdOfW,y+j*thirdOfH,thirdOfW,thirdOfH,depth-1,color)

    
#sierpinskiIterative() draw Sierpinski rectangle by drawing blank (background color) spaces for each depth stage
#x,y - bottom left corner coordinates
#w,h - rectangle width and height
#depth - number of subdividing operations which produce cutout in the middle of rectangle
#color - rectangle color
#background - color of rectangle cutout places
#ignoreUnderHalfPixel - stop iterating depth and return when any of cutout dimmensions gets bellow 0.5 pixel. 
#                       May produce invalid result!
def sierpinskiIterative(x,y,w,h,depth,color, background, ignoreUnderHalfPixel=False):
    if(depth<0):
        raise Exception("Depth cannot be negative")
    
    rectangle(x,y,w,h,color)
    
    thirdOfW=w
    thirdOfH=h
    
    for it in range(depth):
        #Descriptive names?
        prevThirdOfW=thirdOfW  
        prevThirdOfH=thirdOfH
        thirdOfW/=3
        thirdOfH/=3
        
        #We could check if any of rectangle dimmensions is smaller than i.e. 0.5 pixel
        #and then return, because what's the point of sub-pixel calculations? 
        #It wouldn't be displayed. 
        #Can be copied to recurrent sierpinski version too.
        if ignoreUnderHalfPixel:
            if thirdOfW < PIXEL_HALF_SIZE[0] or thirdOfW < PIXEL_HALF_SIZE[1]:
                return
        
        for i in range(3**it):
            for j in range(3**it):
                
                #Tried Bethesda programming style, doesn't work. 
                #This code returns lines with "[[[0. 0. 0.]]]", except we don't use black color anywhere.
                #Do You have any clue why? Will be thankful for any hints.
                #
                #c = glReadPixelsf(x+thirdOfW+i*prevThirdOfW, y+thirdOfH+j*prevThirdOfH, math.copysign(1.0,thirdOfW), math.copysign(1.0,thirdOfH), GL_RGB)
                #print(c)
                #if c == [[background]]:
                #    continue
                rectangle(x+thirdOfW+i*prevThirdOfW,y+thirdOfH+j*prevThirdOfH,thirdOfW,thirdOfH,background)

#render - prepare scene content
def render(time):
    global THREADPOOL
    
    glClear(GL_COLOR_BUFFER_BIT)
    
    #rectangles with deformation, 
    #normal
    rectangle(-70,60,50,30)
    #valid, slightly under each Sierpinski rectangle version
    rectangle(-70,-20,50,40,deform=3)
    #vegetabl... I mean invalid, the feature :)
    rectangle(-70,-60,50,30,deform=-2.1)
    
    fractDepth = 4
    sierpinski(10,10,90,90,fractDepth,[0x33/0xFF,1.0,1.0])
    sierpinskiIterative(10,-80,90,90,fractDepth,[0x99/0xFF,0x99/0xFF,0],[.5,.5,.5])
    

    glFlush()

#update_viewport() - callback used to rescale scene to fit viewport
def update_viewport(window, width, height):
    if height == 0:
        height = 1
    if width == 0:
        width = 1

    global PIXEL_HALF_SIZE
    PIXEL_HALF_SIZE=[200/width/2,200/height/2]
    
    glMatrixMode(GL_PROJECTION)
    glViewport(0, 0, width, height)
    glLoadIdentity()

    glOrtho(-100, 100, -100, 100, 1.0, -1.0)
    
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()


def main():
    if not glfwInit():
        sys.exit(-1)

    window = glfwCreateWindow(400, 400, __file__, None, None)
    if not window:
        glfwTerminate()
        sys.exit(-1)

    glfwMakeContextCurrent(window)
    glfwSetFramebufferSizeCallback(window, update_viewport)
    glfwSwapInterval(1)
    
    startup()
    
    while not glfwWindowShouldClose(window):
        render(glfwGetTime())
        glfwSwapBuffers(window)
        glfwPollEvents()
    shutdown()

    glfwTerminate()


if __name__ == '__main__':
    main() 
