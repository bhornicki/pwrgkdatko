#!/usr/bin/env python3
#It just works.
import sys

from glfw.GLFW import *

from OpenGL.GL import *
from OpenGL.GLU import *

from PIL import Image

import numpy as matlab2
import math

viewer = [0.0, 0.0, 15.0]

pyramid_pos = matlab2.array([-5., 0., 0.])
egg_pos = matlab2.array([5., -5., 0.])
egg_N = 19
egg_points = matlab2.zeros((egg_N, egg_N, 3))
egg_normals = matlab2.zeros((egg_N, egg_N, 3))

theta = 0.0
pix2angle = 1.0

left_mouse_button_pressed = 0
mouse_x_pos_old = 0
delta_x = 0
hide_wall = False
texture_set=0

mat_ambient = [1.0, 1.0, 1.0, 1.0]
mat_diffuse = [1.0, 1.0, 1.0, 1.0]
mat_specular = [1.0, 1.0, 1.0, 1.0]
mat_shininess = 20.0

light_ambient = [0.5, 0.5, 0.0, 1.0]
light_diffuse = [0.8, 0.8, 0.8, 1.0]
light_specular = [1.0, 1.0, 1.0, 1.0]
light_position = [0.0, 0.0, 10.0, 1.0]

att_constant = 1.0
att_linear = 0.05
att_quadratic = 0.001


#side wall as an equilateral triangle
coords_top = matlab2.array([0,0,math.sqrt((5*math.sqrt(3))**2-5**2)]) + pyramid_pos
coords_base = matlab2.array([
        [-5,-5,0],   #LB
        [5,-5,0],   #RB
        [5,5,0],    #RT
        [-5,5,0]   #LT
    ]) + pyramid_pos

normals = matlab2.zeros([5,3])

coords_base_order = [
        0,
        3,
        1,
        2
    ]

tex_file = "lab6_tex_atlas.tga"
tex_rectangle = matlab2.array([
        [0, 0],
        [1, 0],
        [1, 1],
        [0, 1]
    ]) / 4

tex_triangle = matlab2.array([
        [0, 0],
        [1, 0],
        [.5, 1]
    ]) / 4

tex_triangle_upside_down = matlab2.array([
        [0, 1],
        [1, 1],
        [.5, 0]
    ]) / 4

tex_pyramid_offsets = [
    [
        [0,1/2] + tex_rectangle*2,    #base
        [0, 0] + tex_triangle,
        [1/4, 0] + tex_triangle,
        [1/8, 0] + tex_triangle_upside_down,
        [1/8, 1/4] + tex_triangle
        
    ],
    [
        [2/4, 1/4] + tex_rectangle,
        [2/4, 2/4] + tex_triangle,
        [3/4, 1/4] + tex_triangle,
        [2/4, 0]   + tex_triangle,
        [3/4, 2/4] + tex_triangle
    ]
    ]
tex_egg =matlab2.array([1/2,3/4]) 

def calculate_normals():
    global normals
    normals[0] = [0,0,-1]
    for i in range(4):
        a = coords_top - coords_base[i]
        b = coords_top - coords_base[(i+1)%4]
        print(a,b)
        normals[i+1] = (matlab2.cross(a,b))
        normals[i+1] /=  (normals[i+1]**2).sum()**0.5 
    print(normals)

#calculate egg points and normal vectors
#this function just asks for multithreading
def calculate_egg():
    global egg_points
    global egg_normals
    global egg_N
    
    #Uh... I'm not giving up, I'm creating temporary fix :)
    if(egg_N%2 == 0):
        egg_N += 1
        print("even egg vertex count not supported, using {} vertices instead".format(egg_N))
        egg_points = matlab2.zeros((egg_N, egg_N, 3))
        egg_normals = matlab2.zeros((egg_N, egg_N, 3))
        
    N = egg_N
    u = matlab2.zeros(N)
    v = matlab2.zeros(N)
    tmp=1/(N-1)
    for i in range(N):
        u[i] = tmp*i
        v[i] = tmp*i
    for i in range(N):
        for j in range(N):
            tu = u[i]
            tv = v[j]
            piv = math.pi * tv
            spiv = math.sin(piv)
            cpiv = math.cos(piv)
            #points
            xz = -90 * tu**5 + 225 * tu**4 - 270 * tu**3 + 180 * tu**2 - 45 * tu
            egg_points[i][j] = [xz * cpiv,
                                160 * tu**4 - 320 * tu**3 + 160 * tu**2,
                                xz * spiv]
            
            #normals
            temp = (-450 * tu**4 + 900 * tu**3 - 810 * tu**2 + 360 * tu - 45)
            
            xu = temp * cpiv
            xv = math.pi * -xz * spiv
            yu = 640 * tu**3 - 960 * tu**2 + 320 * tu
            yv = 0
            zu = temp * spiv
            zv = math.pi * xz * cpiv
            
            egg_normals[i][j] = [yu*zv - zu*yv,
                                 zu*xv - xu*zv,
                                 xu*yv - yu*xv]
            
            #Who doesn't love brute force?
            if(i==0 or i==N-1):
                egg_normals[i][j] = [0., -1., 0.]
            elif(N%2 and i==math.floor(N/2)):
                egg_normals[i][j] = [0., 1., 0.]
            else:
                #make it an unit vector
                egg_normals[i][j] /=  (egg_normals[i][j]**2).sum()**0.5 
                
            if(i>=N/2 and i != N-1):
                egg_normals[i][j] *= -1
    
    egg_points += egg_pos

def jajo():
    for i in range(math.floor(egg_N/2)):
        glBegin(GL_TRIANGLE_STRIP)
        for j in range(math.floor(egg_N)):
            glNormal3fv(egg_normals[i][j])
            glTexCoord2fv(tex_egg + [j/egg_N * 1/4, i/egg_N*2 * 1/4])
            glVertex3fv(egg_points[i][j])
            
            glNormal3fv(egg_normals[i+1][j])
            glTexCoord2fv(tex_egg + [j/egg_N * 1/4, (i+1)/egg_N*2 * 1/4])
            glVertex3fv(egg_points[i+1][j])
        glEnd()
        
    #...
    for i in range(math.floor(egg_N/2), egg_N-1):
        glBegin(GL_TRIANGLE_STRIP)
        for j in range(math.floor(egg_N)):
            
            glNormal3fv(egg_normals[i+1][j])
            glTexCoord2fv(tex_egg + [1/4,0] + [(j-1)/egg_N * 1/4, (egg_N-i-2)/egg_N*2 * 1/4])
            glVertex3fv(egg_points[i+1][j])
            
            glNormal3fv(egg_normals[i][j])
            glTexCoord2fv(tex_egg + [1/4,0] + [(j-1)/egg_N * 1/4, (egg_N-i-1)/egg_N*2 * 1/4])
            glVertex3fv(egg_points[i][j])
            
        glEnd()


def startup():
    calculate_normals()
    calculate_egg()
    update_viewport(None, 640, 480)
    glClearColor(0.0, 0.0, 0.0, 1.0)
    glEnable(GL_DEPTH_TEST)

    glMaterialfv(GL_FRONT, GL_AMBIENT, mat_ambient)
    glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_diffuse)
    glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular)
    glMaterialf(GL_FRONT, GL_SHININESS, mat_shininess)

    glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient)
    glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse)
    glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular)
    glLightfv(GL_LIGHT0, GL_POSITION, light_position)

    glLightf(GL_LIGHT0, GL_CONSTANT_ATTENUATION, att_constant)
    glLightf(GL_LIGHT0, GL_LINEAR_ATTENUATION, att_linear)
    glLightf(GL_LIGHT0, GL_QUADRATIC_ATTENUATION, att_quadratic)

    glShadeModel(GL_SMOOTH)
    glEnable(GL_LIGHTING)
    glEnable(GL_LIGHT0)

    glEnable(GL_TEXTURE_2D)
    glEnable(GL_CULL_FACE)
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)

    image = Image.open(tex_file)

    glTexImage2D(
        GL_TEXTURE_2D, 0, 3, image.size[0], image.size[1], 0,
        GL_RGB, GL_UNSIGNED_BYTE, image.tobytes("raw", "RGB", 0, -1)
    )
    
    print("Press V to hide one wall")
    print("Press SPACE to toggle texture set")

def shutdown():
    pass


def render(time):
    global theta

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    glLoadIdentity()

    gluLookAt(viewer[0], viewer[1], viewer[2],
              0.0, 0.0, 0.0, 0.0, 1.0, 0.0)

    if left_mouse_button_pressed:
        theta += delta_x * pix2angle + 360
        theta %=360

    glRotatef(theta, 0.0, 1.0, 0.0)

    #Poor man's rectangle
    glNormal3fv(normals[0])
    glBegin(GL_TRIANGLE_STRIP)
    for i in coords_base_order:
        glTexCoord2fv(tex_pyramid_offsets[texture_set][0][i])
        glVertex3fv(coords_base[i])
    glEnd()
    
    for i in range(4):
        if i==3 and hide_wall:
            continue
        glNormal3fv(normals[i+1])
        glBegin(GL_TRIANGLES)
        glTexCoord2fv(tex_pyramid_offsets[texture_set][i+1][0])
        glVertex3fv(coords_base[i])
        glTexCoord2fv(tex_pyramid_offsets[texture_set][i+1][1])
        glVertex3fv(coords_base[(i+1)%4])
        glTexCoord2fv(tex_pyramid_offsets[texture_set][i+1][2])
        glVertex3fv(coords_top)
        glEnd()

    jajo()
    
    glFlush()


def update_viewport(window, width, height):
    global pix2angle
    pix2angle = 360.0 / width

    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()

    gluPerspective(70, 4/3, 0.1, 300.0)

    if width <= height:
        glViewport(0, int((height - width) / 2), width, width)
    else:
        glViewport(int((width - height) / 2), 0, height, height)

    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()


def keyboard_key_callback(window, key, scancode, action, mods):
    global hide_wall
    global texture_set
    if action != GLFW_PRESS:
        return
    if key == GLFW_KEY_ESCAPE:
        glfwSetWindowShouldClose(window, GLFW_TRUE)
    elif key == GLFW_KEY_V:
        hide_wall = not hide_wall
    elif key == GLFW_KEY_SPACE:
        texture_set += 1
        texture_set %= 2


def mouse_motion_callback(window, x_pos, y_pos):
    global delta_x
    global mouse_x_pos_old

    delta_x = x_pos - mouse_x_pos_old
    mouse_x_pos_old = x_pos


def mouse_button_callback(window, button, action, mods):
    global left_mouse_button_pressed

    if button == GLFW_MOUSE_BUTTON_LEFT and action == GLFW_PRESS:
        left_mouse_button_pressed = 1
    else:
        left_mouse_button_pressed = 0


def main():
    if not glfwInit():
        sys.exit(-1)

    #COB and other Mr Laiho projects changed many people life forever
    window = glfwCreateWindow(640, 480, "Bartosz Hornicki: Alexi, You'll be missed", None, None)    
    if not window:
        glfwTerminate()
        sys.exit(-1)

    glfwMakeContextCurrent(window)
    glfwSetFramebufferSizeCallback(window, update_viewport)
    glfwSetKeyCallback(window, keyboard_key_callback)
    glfwSetCursorPosCallback(window, mouse_motion_callback)
    glfwSetMouseButtonCallback(window, mouse_button_callback)
    glfwSwapInterval(1)

    startup()
    nextTime=glfwGetTime()+.5
    do = True
    while not glfwWindowShouldClose(window):
        render(glfwGetTime())
        glfwSwapBuffers(window)
        glfwPollEvents()
        if(texture_set == 1 and (theta>95 and theta<265) and glfwGetTime() > nextTime):
            nextTime = glfwGetTime()+.5
            print("https://youtu.be/dQw4w9WgXcQ")
            if(do):
                glfwSetWindowTitle(window, "Bartosz Hornicki: https://youtu.be/dQw4w9WgXcQ");
                do = False
    shutdown()

    glfwTerminate()


if __name__ == '__main__':
    main()
